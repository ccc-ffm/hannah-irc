'''
Created on 4 Mar 2015

@author: wonko
'''

import ssl
# import irc.client
from irc.connection import Factory

from irc.bot import SingleServerIRCBot, ServerSpec

import signal


class IRCBot(SingleServerIRCBot):
    def __init__(self, server, nickname, channel, **connect_params):
        SingleServerIRCBot.__init__(self, server, nickname, nickname, **connect_params)
        
        self.channel = channel
        

    def on_welcome(self, c, e):
        c.join(self.channel)
        
    def on_pubmsg(self, c, e):
        msg = e.arguments[0].split(' ')
        if msg[0].startswith('!'):
            
            cmd = msg[0].lstrip('!')
            print "a command! " + cmd
            usernick = e.source.partition('!')[0]
            self.do_command(cmd, msg, usernick)
                        
    def do_command(self, cmd, args, nick):  
        c = self.connection
        if cmd == 'help':
            c.notice(nick, "I am New. I dont know what I can do yet!")
        elif cmd == 'join':
            c.notice(nick, "I am pretty shure I should join you somewhere. But dont know how yet")
        elif cmd == 'prejoin':
            c.notice(nick, "I am pretty shure I should expect you somewhere. ")
        elif cmd == 'leave':
            c.notice(nick, "Er.... OK ? bye then ?! ")
        elif cmd == 'open':
            c.notice(nick, "Mmmh.. a window maybe?")
        elif cmd == 'close':
            c.notice(nick, "U sure? I haven't opened anything here.")
        else:
            c.notice(nick, "I don't know what you mean by " + cmd + " are you trying to trick me?")


class BotInstance():

        
        
    def start(self):
        print 'configuring bot'
        server = ServerSpec('irc.hackint.org', 9999)
        ssl_factory = Factory(wrapper=ssl.wrap_socket)
        self.bot = IRCBot([server], 'hannah21', '#chaos.expert', connect_factory=ssl_factory)
        print "starting bot"                
        self.bot.start()
        
    def stop(self,msg):
        print "Stopping Bot"
        self.bot.connection.disconnect(msg)
        
  
  
stopsignals = [signal.SIGTERM, signal.SIGABRT, signal.SIGQUIT]
reloadsignals = [signal.SIGHUP, signal.SIGUSR1, signal.SIGUSR2]
          
def sighandler(signum,frame):
    msg = "I got signal " + str(signum)
    
    if not bot:
        print "It seems there is no bot"
        return
    
    if signum in stopsignals:
        msg += " Stopping."
        bot.stop(msg)
    elif signum in reloadsignals:
        msg += " Reloading"
        bot.stop(msg)
        bot.start()
    else:
        msg += " I dont know. Better stop then."
        bot.stop()
    
        
if __name__ == '__main__':
    global bot 
    bot = BotInstance()
    
    print "registering signals"
    for sig in stopsignals + reloadsignals:
        signal.signal(sig, sighandler)
        
    bot.start()
